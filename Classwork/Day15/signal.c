#include <stdio.h>
#include <unistd.h>
#include <signal.h>

// signal handling demo

//step1: implement signal handler function
void sigint_handler(int sig) {
	printf("signal is caught: %d\n", sig);
}

int main() {
	int i=0;
	//step2: register signal handler (in signal handler table of current process).
	signal(SIGINT, sigint_handler);
	while(1) {
		printf("loop: %d\n", ++i);
		sleep(1);
	}
	return 0;
}


