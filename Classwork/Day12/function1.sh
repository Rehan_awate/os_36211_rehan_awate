#!/bin/bash

# write a function to subtract two numbers

function subtract()
{
	res=`expr $1 - $2`
	echo "$res"
}


function multiply()
{
	res=`expr $1 \* $2`
	echo "$res"
}

echo -n "enter two numbers: "
read num1 num2

result=$(subtract $num1 $num2)
echo "result = $result"

multiply $num1 $num2

